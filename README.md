# Netfilms

Netfilms is a movie application built with Laravel.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have Composer and PHP 8.1 installed on your machine.

### Installation

1. Clone the repository
```bash
git clone https://gitlab.com/marvinhertsoen/backend_test_laravel.git
```

2. Navigate into the project directory
```bash
cd backend_test_laravel
```

3. Place the received .env file
```bash
cp /path/to/env .env
```

4. Install the dependencies via composer
```bash
composer install
```

5. Start the application
```bash
vendor/bin/sail up
```

6. Run the migrations and seed the database through sail
```bash
vendor/bin/sail artisan migrate --seed
```

7. Import movies from the TMDB API through sail (optional)
```bash
vendor/bin/sail artisan app:refresh-movies
```

### Setting up the Cron Job

To refresh the movies daily, set up the following cron job on your production environment:

```bash
0 8 * * * php artisan app:refresh-movies
```

## Usage

- Visit the Trending Movies page at [http://localhost/movies](http://localhost/movies)
- Access the Admin page at [http://localhost/admin](http://localhost/admin). Use the following credentials for login:
  - Username: admin
  - Password: admin


## References and Resources

### Backend
- [Laravel Documentation](https://laravel.com/docs/10.x/readme)
- [Laravel Sail](https://laravel.com/docs/10.x/sail)
- [Laravel and Repository Pattern](https://www.atrakeur.com/blog/web/laravel-et-pattern-repository/)
- [encore/laravel-admin](https://github.com/z-song/laravel-admin)

### Frontend
- [Bootstrap Ecommerce Products List](https://bootstrap-ecommerce.com/products-list.html)
- [Bootstrap Ecommerce Products Detail](https://bootstrap-ecommerce.com/products-detail.html)

### Previous Laravel project
- [Laravel Project](https://gitlab.com/marvinhertsoen/webservice_project)


## License

This project is licensed under the MIT License