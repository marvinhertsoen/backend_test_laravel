    <footer class="text-center text-lg-start text-muted bg-primary mt-3">
        <!-- Section: Links  -->
        <section class="">
            <div class="container text-center text-md-start pt-4 pb-4">
                <!-- Grid row -->
                <div class="row mt-3">
                    <!-- Grid column -->
                    <div class="col-12 col-lg-3 col-sm-12 mb-2">
                        <!-- Content -->
                        <a href="https://mdbootstrap.com/" target="_blank" class="text-white h2">
                            NetFilms
                        </a>
                        <p class="mt-1 text-white">
                            © 2023 Copyright: BackendTestLaravel
                        </p>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-6 col-sm-4 col-lg-2">
                        <!-- Links -->
                        <h6 class="text-uppercase text-white fw-bold mb-2">
                            Praesent
                        </h6>
                        <ul class="list-unstyled mb-4">
                            <li><a class="text-white-50" href="#">Vivamus</a></li>
                            <li><a class="text-white-50" href="#">Tristique</a></li>
                            <li><a class="text-white-50" href="#">Fusce ac</a></li>
                            <li><a class="text-white-50" href="#">Integer sit</a></li>
                        </ul>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-6 col-sm-4 col-lg-2">
                        <!-- Links -->
                        <h6 class="text-uppercase text-white fw-bold mb-2">
                            Etiam
                        </h6>
                        <ul class="list-unstyled mb-4">
                            <li><a class="text-white-50" href="#">Morbi justo</a></li>
                            <li><a class="text-white-50" href="#">Aenean</a></li>
                            <li><a class="text-white-50" href="#">Pharetra</a></li>
                            <li><a class="text-white-50" href="#">Mauris</a></li>
                        </ul>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-6 col-sm-4 col-lg-2">
                        <!-- Links -->
                        <h6 class="text-uppercase text-white fw-bold mb-2">
                            Fusce augue
                        </h6>
                        <ul class="list-unstyled mb-4">
                            <li><a class="text-white-50" href="#">Felis</a></li>
                            <li><a class="text-white-50" href="#">Scelerisque</a></li>
                            <li><a class="text-white-50" href="#">Pellentesque id</a></li>
                            <li><a class="text-white-50" href="#">Condimentum</a></li>
                        </ul>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-6 col-sm-4 col-lg-2">
                        <!-- Links -->
                        <h6 class="text-uppercase text-white fw-bold mb-2">
                            Venenatis
                        </h6>
                        <ul class="list-unstyled mb-4">
                            <li><a class="text-white-50" href="#">Aliquam</a></li>
                            <li><a class="text-white-50" href="#">Sem magna</a></li>
                            <li><a class="text-white-50" href="#">Aenean tempus</a></li>
                            <li><a class="text-white-50" href="#">Sapien</a></li>
                        </ul>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
        </section>
        <!-- Section: Links  -->

       <!-- Footer -->
        @livewireScripts
        <script src="https://mdbootstrap.com/api/snippets/static/download/MDB5-Free_6.4.2/js/mdb.min.js"></script>
        <script></script>
        <script type="text/javascript"
            src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/plugins/mdb-plugins-gathered.min.js"></script>
    </body>
</html>
