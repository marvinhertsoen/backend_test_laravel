<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>{{$metaTitle ?? 'Netfilms'}}</title>
        @livewireStyles
        <style>
        body {
            margin: 0;
            padding: 0;
        }
    </style>
    <!-- style.css -->
    <link rel="icon" type="image/png" href="{{ asset('images/netfilms_logo.png') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&amp;display=swap">
    <link rel="stylesheet" type="text/css"
        href="https://mdbootstrap.com/api/snippets/static/download/MDB5-Free_6.4.2/css/mdb.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/mdb-plugins-gathered.min.css">
    <style>
        .icon-hover:hover {
            border-color: #3b71ca !important;
            background-color: white !important;
        }

        .icon-hover:hover i {
            color: #3b71ca !important;
        }
    </style>
    <style>
        INPUT:-webkit-autofill,
        SELECT:-webkit-autofill,
        TEXTAREA:-webkit-autofill {
            animation-name: onautofillstart
        }

        INPUT:not(:-webkit-autofill),
        SELECT:not(:-webkit-autofill),
        TEXTAREA:not(:-webkit-autofill) {
            animation-name: onautofillcancel
        }

        @keyframes onautofillstart {}

        @keyframes onautofillcancel {}
    </style>
</head>

<body>
<!--Main Navigation-->
<header>
    <!-- Jumbotron -->
    <div class="p-3 text-center bg-white border-bottom">
        <div class="container">
            <div class="row gy-3">
                <!-- Left elements -->
                <div class="col-lg-2 col-sm-4 col-4">
                    <a href="/movies" class="float-start">
                        <img src="{{asset('images/netfilms.png')}}" height="35">
                    </a>
                </div>
                <!-- Left elements -->

                <!-- Center elements -->
                <div class="order-lg-last col-lg-5 col-sm-8 col-8">
                    <div class="d-flex float-end">
                        <a href="/admin/auth/login"
                            class="me-1 border rounded py-1 px-3 nav-link d-flex align-items-center" target="_blank"> <i
                                class="fas fa-user-alt m-1 me-md-2"></i>
                            <p class="d-none d-md-block mb-0">Sign in</p>
                        </a>
                    </div>
                </div>
                <!-- Center elements -->

                <!-- Right elements -->
                <div class="col-lg-5 col-md-12 col-12">
                    <form action="/search" method="POST" class="input-group float-center">
                            @csrf
                            <div class="form-outline">
                                <input name="searchTerm" type="search" id="form1" class="form-control">
                                <label class="form-label" for="form1" style="margin-left: 0px;">Find a movie</label>
                                <div class="form-notch">
                                    <div class="form-notch-leading" style="width: 9px;"></div>
                                    <div class="form-notch-middle" style="width: 47.2px;"></div>
                                    <div class="form-notch-trailing"></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary shadow-0">
                                <i class="fas fa-search"></i>
                            </button>
                    </form>
                </div>
                <!-- Right elements -->
            </div>
        </div>
    </div>
    <!-- Jumbotron -->


