        @include('top')
        <!-- Heading -->
        <div class="">
            <div class="container py-4">
                <!-- Breadcrumb -->
                <nav class="d-flex">
                    <h6 class="mb-0">
                    <a class="btn btn-primary shadow-0" type="button" href="/movies" style="">&lt; Back</a>
                    </h6>
                </nav>
                <!-- Breadcrumb -->
            </div>
        </div>
        <!-- Heading -->
    </header>

    <!-- content -->
    <section class="py-5">
        <div class="container">
            <div class="row gx-5">
                <aside class="col-lg-6">
                    <div class="image-container border rounded-4 mb-3 d-flex justify-content-center">
                        <a data-fslightbox="mygalley" class="rounded-4" target="_blank" data-type="image"
                            href="{{ $movie->poster_url  }}">
                            <img style="max-width: 100%; max-height: 100vh; margin: auto;" class="rounded-4 fit"
                                src="{{ $movie->poster_url   }}">
                        </a>
                    </div>
                </aside>
                <main class="col-lg-6">
                    <div class="ps-lg-3">
                        <h4 class="title text-dark">
                            {{ $movie->title }}
                        </h4>
                        <div class="d-flex flex-row my-3">
                            @include('movies.components.stars', ['ratingOutOf5' => round($movie->vote_average /2, 1)])
                            <span class="text-muted"><i class="fas fa-user fa-sm mx-1"></i>{{ $movie->vote_count}} 
                                reviews</span>
                        </div>

                        <p>
                            {{ $movie->overview }}
                        </p>

                        <div class="row">
                            <dt class="col-3">Original Title:</dt>
                            <dd class="col-9">{{$movie->original_title}}</dd>

                            <dt class="col-3">Original Language:</dt>
                            <dd class="col-9">{{$movie->original_language}}</dd>
                            
                            <dt class="col-3">Release date:</dt>
                            <dd class="col-9">{{date('d/m/Y', strtotime($movie->release_date))}} </dd>
                            
                            <dt class="col-3">Genre:</dt>
                            <dd class="col-9">
                                @include('movies.components.genres', ['categories' => $movie->categories])
                            </dd>

                            <dt class="col-3">Budget:</dt>
                            <dd class="col-9">{{$movie->budget}} USD </dd>
                        </div>

                        <hr>

                        <a href="#" class="btn btn-primary shadow-0"> <i
                                class="me-1 fa fa-shopping-basket"></i> Buy the movie </a>
                    </div>
                </main>
            </div>
        </div>
    </section>
    <!-- content -->

    <section class="bg-light border-top py-4">
        <div class="container">
            <div class="row gx-4">
                <div class="col-lg-12">
                    <div class="px-0 border rounded-2 shadow-0">
                        <div class="card">
                            @include('movies.components.similarMovies', ['similarMovies' => $similarMovies, 'movie' => $movie])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer -->
@include('bottom')