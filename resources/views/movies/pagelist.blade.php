    @include('top')
    <!-- Heading -->
    <div class="bg-primary mb-4">
        <div class="container py-4">
            <h3 class="text-white mt-2">{{$pageListTitle}}</h3>
            @if (isset($pageListSubtitle))
                <nav class="d-flex mb-2">
                    <h6 class="mb-0">
                        <a href="" class="text-white-50">{{$pageListSubtitle}}</a>
                    </h6>
                </nav>
            @endif
        </div>
    </div>
    <!-- Heading -->
</header>

<!-- sidebar + content -->
<section class="">
    <div class="container">
        <div class="row">
            <!-- content -->
            <div class="col-lg-12">
                <header class="d-sm-flex align-items-center border-bottom mb-4 pb-3">
                    <strong class="d-block py-2">{{$collectionSize}} movies found </strong>
                </header>

                @foreach ($movies as $movie)
                    <div class="movie-container row justify-content-center mb-3">
                        <div class="col-md-12">
                            <div class="card shadow-0 border rounded-3">
                                <div class="card-body">
                                    <div class="row g-0">
                                        <div class="col-xl-3 col-md-4 d-flex justify-content-center">
                                            <div
                                                class="bg-image hover-zoom ripple rounded ripple-surface me-md-3 mb-3 mb-md-0">
                                                <img src="{{$movie->poster_url}}"
                                                    class="w-100">
                                                <a href="/movies/{{$movie->id}}">
                                                    <div class="hover-overlay">
                                                        <div class="mask"
                                                            style="background-color: rgba(253, 253, 253, 0.15);"></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-md-5 col-sm-7">
                                            <h5>{{$movie->title}}</h5>
                                            <div class="d-flex flex-row">
                                                @include('movies.components.stars', ['ratingOutOf5' => round($movie->vote_average /2, 1)])
                                                <span class="text-muted">{{$movie->vote_count}} reviews</span>
                                            </div>

                                            <p class="movie-overview text mb-4 mb-md-0">
                                                {{$movie->overview}}
                                            </p>
                                        </div>
                                        <div class="col-xl-3 col-md-3 col-sm-5">
                                            <h4 class="">Tags</h4>
                                            <h6 class="">
                                                @include('movies.components.genres', ['categories' => $movie->categories])
                                            </h6>
                                            <div class="mt-4">
                                                <a class="btn btn-primary shadow-0" type="button" target="_blank" href="/movies/{{$movie->id}}">more information </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <hr>

                <!-- Pagination -->
                <nav aria-label="Page navigation example" class="d-flex justify-content-center mt-3">
                    <ul class="pagination">
                        <li class="page-item @if($currentPage < 2) disabled @endif">
                            <a class="page-link" href="/movies?page={{$currentPage-1}}" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                            </a>
                        </li>
                        @for ($i = 1; $i <= $maxPage ; $i++)
                            <li class="page-item @if($i == $currentPage) active @endif">
                                <a class="page-link" href="/movies?page={{$i}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="page-item @if($currentPage >= $maxPage) disabled @endif"">
                            <a class="page-link" href="/movies?page={{$currentPage+1}}" aria-label="Next">
                                <span aria-hidden="true">»</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- Pagination -->
            </div>
        </div>
    </div>
</section>
@include('bottom')
