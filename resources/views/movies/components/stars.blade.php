<div class="text-warning mb-1 me-2">
    @for ($i = 0; $i < (int)$ratingOutOf5; $i++)
        <i class="fa fa-star"></i>
    @endfor
    @if (round($ratingOutOf5, 0) != $ratingOutOf5)
        <i class="fas fa-star-half-alt"></i>
    @endif
    <span class="ms-1">
        {{$ratingOutOf5}}
    </span>
</div>
