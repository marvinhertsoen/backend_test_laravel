@if (!empty($similarMovies))
    <div class="card-body row">
        <h5 class="similar-movies-title card-title">Similar movies</h5>
            @foreach ($similarMovies as $movieLike)
            
                <div class="col-sm">
                    <a href="/movies/{{$movieLike->id}}" class="me-3">
                        <img src="{{ $movieLike->poster_url}}"
                            style="min-width: 96px; height: 96px;" class="img-md img-thumbnail">
                    </a>
                    <div class="info">
                        <a href="/movies/{{ $movieLike->id }}" class="nav-link mb-1">
                            {{$movieLike->title}}
                        </a>
                        @include('movies.components.stars', ['ratingOutOf5' => round($movieLike->vote_average /2, 1)])
                        <strong class="text-dark">    
                            @include('movies.components.genres', ['categories' => $movieLike->categories])
                        </strong>
                    </div>
                </div>
            @endforeach
    </div>

@endif