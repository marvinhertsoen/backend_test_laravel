<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'original_language',
        'original_title',
        'overview',
        'poster_url',
        'popularity',
        'vote_average',
        'vote_count',
        'release_date',
        'budget',
    ];

    public function categories()
    {
        return $this->hasMany(MovieCategory::class, 'movie_id');
    }
}
