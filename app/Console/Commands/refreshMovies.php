<?php

namespace App\Console\Commands;

use App\Models\Movie;
use App\Service\TmdbService;
use Exception;
use Illuminate\Console\Command;

class refreshMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:refresh-movies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh movies from TheMovieDb API';

    /**
     * TmdbService
     * 
     * @var TmdbService
     */
    private $tmdbService;

    public function __construct(TmdbService $tmdbService)
    {
        parent::__construct();
        $this->tmdbService = $tmdbService;
    }

    /**
     * Remove all movies and refresh them from TheMovieDb API
     * 
     * @return void 
     */
    public function handle()
    {
        try {
            Movie::all()->each->delete();
            $this->tmdbService->refreshAllMovies();
        } catch (Exception $e) {
            echo ('Error while fetching movies from TheMovieDb API : ' . $e->getMessage());
            return;
        }

        echo ('Movie import finished.' . PHP_EOL);
    }
}
