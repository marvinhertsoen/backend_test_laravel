<?php

use Illuminate\Routing\Router;
use App\Admin\Controllers\MovieController;
use App\Admin\Controllers\MovieCategoryController;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('movies', MovieController::class);
    $router->resource('movieCategories', MovieCategoryController::class);

});
