<?php

namespace App\Admin\Controllers;

use App\Models\Movie;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MovieController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Movie';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Movie());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('original_language', __('Original language'));
        $grid->column('original_title', __('Original title'));
        $grid->column('overview', __('Overview'));
        $grid->column('poster_url', __('Poster url'));
        $grid->column('popularity', __('Popularity'));
        $grid->column('vote_average', __('Vote average'));
        $grid->column('vote_count', __('Vote count'));
        $grid->column('release_date', __('Release date'));
        $grid->column('budget', __('Budget'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('categories', 'Categories count')->display(function ($categories) {
            $count = count($categories);
            return "<span class='label label-warning'>{$count}</span>";
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Movie::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('original_language', __('Original language'));
        $show->field('original_title', __('Original title'));
        $show->field('overview', __('Overview'));
        $show->field('poster_url', __('Poster url'));
        $show->field('popularity', __('Popularity'));
        $show->field('vote_average', __('Vote average'));
        $show->field('vote_count', __('Vote count'));
        $show->field('release_date', __('Release date'));
        $show->field('budget', __('Budget'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        //show categories
        $show->categories('Categories', function ($categories) {
            $categories->resource('/admin/movieCategories');
            $categories->id();
            $categories->name();
            $categories->created_at();
            $categories->updated_at();
        });
        
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Movie());

        $form->textarea('title', __('Title'))->required();
        $form->textarea('original_language', __('Original language'));
        $form->textarea('original_title', __('Original title'));
        $form->textarea('overview', __('Overview'));
        $form->textarea('poster_url', __('Poster url'));
        $form->decimal('popularity', __('Popularity'));
        $form->decimal('vote_average', __('Vote average'));
        $form->number('vote_count', __('Vote count'));
        $form->date('release_date', __('Release date'))->default(date('Y-m-d'));
        $form->number('budget', __('Budget'));

        $form->hasMany('categories', function (Form\NestedForm $form) {
            $form->text('name');
        });
        return $form;
    }
}
