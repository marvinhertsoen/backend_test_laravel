<?php

namespace App\Admin\Controllers;

use App\Models\Movie;
use App\Models\MovieCategory;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MovieCategoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'MovieCategory';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MovieCategory());

        $grid->column('id', __('Id'));
        $grid->movie_id('Movie')->display(function ($movie_id) {
            $movie = Movie::find($movie_id);
            return sprintf('#%s (%s)', $movie->id, $movie->title);
        });
        $grid->column('name', __('Name'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MovieCategory::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('movie_id', __('Movie id'));
        $show->movie_id('Movie')->as(function ($movie_id) {
            $movie = Movie::find($movie_id);
            return sprintf('#%s (%s)', $movie->id, $movie->title);
        });
        $show->field('name', __('Name'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MovieCategory());

        $form->number('movie_id', __('Movie id'))
            ->default(request()->get('movie_id'));
        $form->textarea('name', __('Name'));

        return $form;
    }
}
