<?php

namespace App\Service;

use App\Models\Movie;
use App\Repository\Eloquent\MovieRepository;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class MovieService implements ICrudService
{
    const SIMILAR_MOVIES_LIMIT = 5;

    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * Get the most popular movies
     * 
     * @param mixed $page 
     * @param mixed $pageSize 
     * @return iterable 
     */
    public function getTrendingMovies($page, $pageSize): iterable
    {
        $movies = Movie::orderBy('popularity', 'desc')->get();
        return $movies->forPage($page, $pageSize);
    }

    /**
     * Get similar movies for a given movie
     * 
     * @param Movie $movie 
     * @param int $limit 
     * @return iterable 
     */
    public function getSimilarMovies(Movie $movie, $limit = self::SIMILAR_MOVIES_LIMIT): iterable
    {
        return Movie::whereHas('categories', function ($query) use ($movie) {
            $query->whereIn('name', $movie->categories->pluck('name'));
        })->where('id', '!=', $movie->id)->orderBy('popularity', 'desc')->take($limit)->get();
    }

    /**
     * Search movies by title
     * 
     * @param mixed $searchTerm 
     * @return iterable 
     */
    public function searchMoviesByTitle($searchTerm): iterable
    {
        return Movie::orderBy('popularity', 'desc')
            ->where('title', 'like', '%' . $searchTerm . '%')->get();
    }

    /**
     * Count all movies
     * 
     * @return int 
     */
    public function count(): int
    {
        return $this->movieRepository->count();
    }


    /* ************************************ */
    /* *************** CRUD *************** */
    /* ************************************ */

    public function create(EloquentModel $entity): EloquentModel
    {
        return $this->movieRepository->insert($entity);
    }

    public function getManyBy($criteria): iterable
    {
        return $this->movieRepository->selectManyBy($criteria);
    }

    public function getAll(): iterable
    {
        return $this->movieRepository->selectAll();
    }

    public function get(int $id): EloquentModel|null
    {
        return $this->movieRepository->selectById($id);
    }

    public function update(EloquentModel $entity): EloquentModel
    {
        return $this->movieRepository->update($entity);
    }

    public function delete(int $id): bool
    {
        return $this->movieRepository->delete($this->get($id));
    }
}
