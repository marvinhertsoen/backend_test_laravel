<?php

namespace App\Service;

use App\Repository\Api\TmdbRepository;
use Exception;

class TmdbService
{
    private $theMovieDbRepository;

    public function __construct(TmdbRepository $theMovieDbRepository)
    {
        $this->theMovieDbRepository = $theMovieDbRepository;
    }

    /**
     * Refresh movies from TheMovieDb API
     * 
     * @return void 
     * @throws Exception 
     */
    public function refreshAllMovies(): void
    {
        $movieGroups = $this->getTrendingMoviesByWeek();

        if (empty($movieGroups)) {
            throw new Exception('No movies found');
        }

        foreach ($movieGroups as $movieGroup) {
            $movieGroup->movie->save();
            $movieGroup->movie->categories()->saveMany($movieGroup->categories);
        }
    }

    /**
     * Get trending movies for the current day
     * 
     * @return array 
     * @throws Exception 
     */
    public function getTrendingMoviesByDay(): array
    {
        return $this->theMovieDbRepository->getTrendingMoviesGroup('day');
    }

    /**
     * Get trending movies for the current week
     * 
     * @return array 
     * @throws Exception 
     */
    public function getTrendingMoviesByWeek(): array
    {
        return $this->theMovieDbRepository->getTrendingMoviesGroup('week');
    }
}
