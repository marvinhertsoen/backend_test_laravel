<?php

namespace App\Service;

use Illuminate\Database\Eloquent\Model;

interface ICrudService
{
    public function create(Model $entity): Model;
    public function getAll(): iterable;
    public function get(int $id): Model|null;
    public function update(Model $model): Model;
    public function delete(int $id): bool;
}
