<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMovieRequest;
use App\Http\Requests\UpdateMovieRequest;
use App\Models\Movie;
use App\Service\MovieService;
use Illuminate\View\View;
use Illuminate\Contracts\Container\BindingResolutionException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MovieController extends Controller
{
    const PAGE_SIZE = 5;

    private $movieService;

    public function __construct(MovieService $movieService)
    {
        $this->movieService = $movieService;
    }

    /**
     * The PageList action
     * 
     * @return View 
     * @throws BindingResolutionException 
     * @throws BadRequestException 
     */
    public function index(): \Illuminate\View\View
    {
        $page = request()->get('page', 1);
        $collectionSize = $this->movieService->count(); // count all movies
        $movies = $this->movieService->getTrendingMovies($page, self::PAGE_SIZE);

        return view('movies.pagelist', [
            'pageListTitle' => 'The most popular',
            'movies' => $movies,
            'collectionSize' =>  $collectionSize,
            'currentPage' => $page,
            'maxPage' => ceil($collectionSize / self::PAGE_SIZE),
        ]);
    }

    /**
     * The Movie Details action
     * 
     * @param int $entityId 
     * @return View 
     * @throws HttpException 
     * @throws NotFoundHttpException 
     * @throws BindingResolutionException 
     */
    public function details(int $entityId): \Illuminate\View\View
    {
        $movie = $this->movieService->get($entityId);
        if (!$movie)
            abort(404);

        $similarMovies = $this->movieService->getSimilarMovies($movie, 5);

        return view('movies.details', [
            'metaTitle' => 'Netfilms - ' . $movie->title,
            'movie' => $movie,
            'similarMovies' => $similarMovies,
        ]);
    }

    /**
     * The Search action
     * 
     * @return View 
     * @throws BindingResolutionException 
     * @throws BadRequestException 
     */
    public function search(): \Illuminate\View\View
    {
        $searchTerm = request()->get('searchTerm', null);

        if (!$searchTerm)
            return redirect('movies');

        $movies = $this->movieService->searchMoviesByTitle($searchTerm);
        $collectionSize = count($movies);

        return view('movies.pagelist', [
            'pageListTitle' => 'Results',
            'pageListSubtitle' => 'Search term : "' . $searchTerm . '"',
            'movies' => $movies,
            'collectionSize' => $collectionSize,
            'currentPage' => 1,
            'maxPage' => ceil($collectionSize / self::PAGE_SIZE),
        ]);
    }
}
