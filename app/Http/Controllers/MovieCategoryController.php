<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMovieCategoryRequest;
use App\Http\Requests\UpdateMovieCategoryRequest;
use App\Models\MovieCategory;

class MovieCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMovieCategoryRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(MovieCategory $movieCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MovieCategory $movieCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMovieCategoryRequest $request, MovieCategory $movieCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MovieCategory $movieCategory)
    {
        //
    }
}
