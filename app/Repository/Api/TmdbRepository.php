<?php

namespace App\Repository\Api;

use App\Models\Movie;
use App\Models\MovieCategory;
use Exception;
use Illuminate\Support\Facades\Http;
use stdClass;

class TmdbRepository
{
    const TRENDING_PERIOD_DAY = 'day';
    const TRENDING_PERIOD_WEEK = 'week';

    const TMDB_BASE_URL = 'https://api.themoviedb.org/3/';

    private $bearerToken;

    public function __construct()
    {
        $this->bearerToken = env('TMDB_BEARER_TOKEN');
    }

    /**
     * Get trending movies group on a specific period
     * 
     * @param string $period 
     * @return array 
     * @throws Exception 
     */
    public function getTrendingMoviesGroup($period = self::TRENDING_PERIOD_DAY): array
    {
        $movieIds = $this->getTrendingMoviesIds($period);
        $movies = [];
        foreach ($movieIds as $movieId) {
            try{
                $movie = $this->getMovieGroupById($movieId);
            } catch (Exception $e) {
                continue;
            }

            $movies[] = $movie;
        }
        return $movies;
    }

    /**
     * Get movie group by movie id
     * 
     * @param mixed $movieId 
     * @return Movie|null 
     * @throws Exception 
     */
    private function getMovieGroupById($movieId): stdClass
    {
        $response = Http::withToken($this->bearerToken)->get(self::TMDB_BASE_URL . 'movie/' . $movieId . '?language=en-US');
        $rawMovieDetails = json_decode($response, true);

        if ($rawMovieDetails['status_code'] ?? null)
            throw new Exception('Error while fetching movie details from TheMovieDb API : ' . $rawMovieDetails['status_message'] ?? null);
            // return null


        $movieGroup = new StdClass();
        $movieGroup->movie = $this->morphMovie($rawMovieDetails);;
        $movieGroup->categories = $this->morphMovieCategory($rawMovieDetails['genres'] ?? []);

        return $movieGroup;
    }

    /**
     * Get the id list of trending movies
     * 
     * @param string $period 
     * @return array 
     * @throws Exception 
     */
    private function getTrendingMoviesIds($period): array
    {
        $response = Http::withToken($this->bearerToken)->get(self::TMDB_BASE_URL . '/trending/all/' . $period . '?language=en-US');
        $rawMovies = json_decode($response, true);
        $movieIds = array_column($rawMovies['results'], 'id');

        return $movieIds;
    }

    /**
     * Morph a raw movie to a Movie model
     * 
     * @param mixed $rawMovie 
     * @return Movie 
     */
    private function morphMovie($rawMovie): Movie
    {
        $posterUrl = isset($rawMovie['poster_path']) ?
            'https://image.tmdb.org/t/p/w400' . $rawMovie['poster_path'] :
            'https://lesvertsrdc.org/wp-content/uploads/2023/07/istockphoto-1147544807-612x612-1.jpg';

        $movie = new Movie([
            'title' => $rawMovie['title'],
            'original_language' => $rawMovie['original_language'],
            'original_title' => $rawMovie['original_title'],
            'overview' => $rawMovie['overview'],
            'poster_url' => $posterUrl, //https://developer.themoviedb.org/docs/image-basics
            'popularity' => $rawMovie['popularity'],
            'vote_average' => $rawMovie['vote_average'],
            'vote_count' => $rawMovie['vote_count'],
            'release_date' => $rawMovie['release_date'],
            'budget' => $rawMovie['budget'],
        ]);


        return $movie;
    }

    /**
     * Morph raw movie categories to MovieCategory models
     * 
     * @param mixed $rawMovieCategories 
     * @return array 
     */
    private function morphMovieCategory($rawMovieCategories): array
    {
        $movieCategories = [];
        foreach ($rawMovieCategories as $rawMovieCategory) {
            $movieCategory = new MovieCategory([
                'name' => $rawMovieCategory['name'],
            ]);
            $movieCategories[] = $movieCategory;
        }
        return $movieCategories;
    }
}
