<?php

namespace App\Repository\Eloquent;

use App\Models\Movie;

class MovieRepository extends AbstractRepository
{
    public function __construct(Movie $movie)
    {
        $this->model = $movie;
    }
}