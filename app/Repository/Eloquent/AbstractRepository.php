<?php

namespace App\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function selectAll(): array
    {
        return $this->model->all()->toArray();
    }

    public function selectById(int $id): Model|null
    {
        return $this->model->find($id);
    }

    public function selectOneBy(array $criteria): Model
    {
        return $this->model->where($criteria)->first();
    }

    public function selectManyBy(array $criteria = []): array
    {
        return $this->model->where($criteria)->get()->toArray();
    }

    public function insert(Model|array $model): Model
    {
        if (is_array($model))
            return $this->model->create($model);

        return $model->save();
    }

    public function update(Model $model): Model
    {
        $res = $model->save();
        if (!$res)
            throw new \Exception("Error updating model: " . $model->toJson());

        return $model;
    }

    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    public function count(): int
    {
        return $this->model->count();
    }
}
