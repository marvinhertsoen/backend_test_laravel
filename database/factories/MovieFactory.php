<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Movie>
 */
class MovieFactory extends Factory
{

    const FAKE_POSTER_URL = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/F_for_Fake_%281973_poster%29.jpg/401px-F_for_Fake_%281973_poster%29.jpg';
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->text(50),
            'original_language' => $this->faker->text(50),
            'original_title' => $this->faker->text(50),
            'overview' => $this->faker->text(350),
            'poster_url' => self::FAKE_POSTER_URL,
            'popularity' => $this->faker->randomFloat(3, 0, 1000),
            'vote_average' => $this->faker->randomFloat(3, 6, 10),
            'vote_count' => $this->faker->randomNumber(4),
            'release_date' => $this->faker->date(),
            'budget' => $this->faker->randomNumber(7)
        ];
    }
}
