<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\MovieCategory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Create 1 admin user and configure laravel-admin
        $this->call([
            AdminTablesSeeder::class,
        ]);

        //Create 50 movies with 3 categories each
        Movie::class::factory(50)->create()->each(function ($movie) {
            $movie->categories()->saveMany(MovieCategory::factory(3)->make());
        });
    }
}
