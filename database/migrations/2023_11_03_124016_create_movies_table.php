<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('original_language');
            $table->text('original_title');
            $table->text('overview');
            $table->text('poster_url');
            $table->float('popularity', 8, 3);
            $table->float('vote_average', 8, 3);
            $table->integer('vote_count');
            $table->date('release_date');
            $table->integer('budget');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('movies');
    }
};
